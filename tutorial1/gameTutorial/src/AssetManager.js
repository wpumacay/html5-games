/**
* @constructor
*/
function AssetManager(){

	this.m_successCount = 0;
	this.m_errorCount = 0;
	this.m_cache = {};
	this.m_downloadQueue = [];
	this.m_soundsQueue = [];
}

AssetManager.prototype.queueDownload = function queueDownload( path ) {
	this.m_downloadQueue.push( path );
};

AssetManager.prototype.queueSound = function queueSound( id , path ) {
	this.m_soundsQueue.push( { id:id , path:path } );
};

AssetManager.prototype.downloadAll = function downloadAll( callback ) {
	// Check if there is an asset or sound to download
	if ( this.m_downloadQueue.length === 0 && this.m_soundsQueue.length === 0 ) {
		callback();
	}

	//this.downloadSounds(callback);

	for (var i = 0; i < this.m_downloadQueue.length; i++) {
		var path = this.m_downloadQueue[i];
		var img = new Image();
		var objCaller = this;
		img.addEventListener( "load" , function(){
			console.log( this.src + 'is loaded');
			objCaller.m_successCount += 1;
			if ( objCaller.isDone() ){
				callback();				
			}
		} , false );
		img.addEventListener( "error" , function(){
			console.log( "error loading " + this.src );
			objCaller.m_errorCount += 1;
			if ( objCaller.isDone() ) {
				callback();
			}

		} , false );
		img.src = path;
		this.m_cache[path] = img;
	};

};

AssetManager.prototype.downloadSounds = function downloadSound( callback ) {
	console.log( "loading sounds..." );
};

AssetManager.prototype.downloadSound = function( id , path , callback ) {
	console.log( "loading sound: " );
};

AssetManager.prototype.getAsset = function getAsset( path ) {
	return this.m_cache[path];
};

AssetManager.prototype.isDone = function isDone() {
	return ( this.m_downloadQueue.length + this.m_soundsQueue.length === this.m_successCount + this.m_errorCount );
};

AssetManager.prototype.getClip = function getClip() {
	
};

