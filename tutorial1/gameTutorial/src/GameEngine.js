window.requestAnimFrame = (function(){
      return  window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              window.oRequestAnimationFrame      ||
              window.msRequestAnimationFrame     ||
              function(/* function */ callback, /* DOMElement */ element){
                window.setTimeout(callback, 1000 / 60);
              };
})();

function GameEngine () {
	this.m_entities = [];
	this.m_ctx = null;
	this.m_canvasWidth = null;
	this.m_canvasHeight = null;
	this.m_testAsset = null;
}

GameEngine.prototype.init = function init( ctx ) {
	console.log( "initializing game!!!" );
	this.m_ctx = ctx;
	this.m_canvasWidth = this.m_ctx.canvas.width;
	this.m_canvasHeight = this.m_ctx.canvas.height;	
	console.log( "game initialized..." );
};

GameEngine.prototype.start = function start() {
	console.log( "starting game!!!" );
	var objCaller = this;
	(function gameLoop () {
		objCaller.loop();
		requestAnimFrame( gameLoop , 
						  objCaller.m_ctx.canvas);
	})();
};

GameEngine.prototype.draw = function draw() {
	this.m_ctx.clearRect( 0 , 0 ,
						  this.m_canvasWidth, this.m_canvasHeight );

	this.m_ctx.save();

	if ( this.m_testAsset !== null ) {
		this.m_ctx.drawImage( this.m_testAsset ,
							  this.m_canvasWidth / 2 ,
							  this.m_canvasHeight / 2 );		
	}
	this.m_ctx.restore();
};

GameEngine.prototype.update = function update( dt ) {
	console.log( "updating the game" );
};

GameEngine.prototype.addAsset = function addAsset( assetManager , assetName ) {
	var asset = assetManager.getAsset( assetName );	
};

GameEngine.prototype.addTestAsset = function addTestAsset( assetManager , assetName ) {
	var asset = assetManager.getAsset( assetName );	
	this.m_testAsset = asset;	
	console.log( this.m_testAsset );
	console.log( "testAssetLoaded" );
};

GameEngine.prototype.loop = function() {
	var dt = 25;
	this.update( dt );
	this.draw();
};