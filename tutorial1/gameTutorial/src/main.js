var canvas = document.getElementById( "surface" );
var ctx = canvas.getContext( "2d" );




var game = new GameEngine();
var ASSET_MANAGER = new AssetManager();

ASSET_MANAGER.queueDownload( "img/earth.png" );

ASSET_MANAGER.downloadAll( function () {
	console.log( "assets loaded" );
	game.addTestAsset( ASSET_MANAGER , "img/earth.png" )
	game.init(ctx);
	game.start();
} );

