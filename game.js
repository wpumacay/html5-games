var jsonData = {"frames": {

"bg_1.png":
{
	"frame": {"x":2,"y":2,"w":900,"h":917},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":900,"h":917},
	"sourceSize": {"w":900,"h":917}
},
"enemy_jet_0.png":
{
	"frame": {"x":904,"y":2,"w":40,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":25},
	"sourceSize": {"w":40,"h":25}
},
"jet_0.png":
{
	"frame": {"x":946,"y":2,"w":40,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":25},
	"sourceSize": {"w":40,"h":25}
},
"power_0.png":
{
	"frame": {"x":988,"y":2,"w":10,"h":10},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":10,"h":10},
	"sourceSize": {"w":10,"h":10}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "atlas_0.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:66c75ec529b98d14bd06b635c7b19971$"
}
}




var canvasBg 		= document.getElementById("canvasBg");
var ctxBg			= canvasBg.getContext("2d");

var player;
var enemy1;

var GAME_WIDTH 	= canvasBg.width;
var GAME_HEIGHT = canvasBg.height;

var fps = 1000/40;
var dt 	= 0.025;
var drawInterval;

var canvasJet		= document.getElementById("canvasJet");
var ctxJet			= canvasJet.getContext("2d");

var canvasEnemy		= document.getElementById("canvasEnemy");
var ctxEnemy		= canvasEnemy.getContext("2d");

var clearCanvasBtn 	= document.getElementById("clearCanvasBtn");
clearCanvasBtn.addEventListener("click",clearCanvas,false);

var drawSquareBtn 	= document.getElementById("drawSquareBtn");
drawSquareBtn.addEventListener("click",drawSquare,false);

var imgSprite = new Image();
imgSprite.src = 'images/atlas_0.png';
imgSprite.addEventListener('load',init,false);




function init(){
	drawBg();
	startDrawing();
	player = new Jet();
	enemy1 = new Enemy();
	document.addEventListener('keydown',onKeyDown,false);
	document.addEventListener('keyup',onKeyUp,false);
}

function onKeyDown(e){
	var keyID = (e.keycode) ? e.keyCode : e.which;
	if(keyID === 38 || keyID === 87){
		player.vy = -100.0;	
		console.log("onKeyDown->up");
		e.preventDefault();
	}
	if(keyID === 39 || keyID === 68){
		player.vx = 100.0;	
		console.log("onKeyDown->right");	
		e.preventDefault();
	}
	if(keyID === 40 || keyID === 83){		
		player.vy = 100.0;		
		console.log("onKeyDown->down");
		e.preventDefault();
	}
	if(keyID === 37 || keyID === 65){
		player.vx = -100.0;		
		console.log("onKeyDown->left");
		e.preventDefault();
	}	
}

function onKeyUp(e){
	var keyID = (e.keycode) ? e.keyCode : e.which;
	if(keyID === 38 || keyID === 87){
		player.vy = 0.0;		
		console.log("onKeyUp->up");
		e.preventDefault();
	}
	if(keyID === 39 || keyID === 68){
		player.vx = 0.0;	
		console.log("onKeyUp->right");	
		e.preventDefault();
	}
	if(keyID === 40 || keyID === 83){
		player.vy = 0.0;		
		console.log("onKeyUp->down");
		e.preventDefault();
	}
	if(keyID === 37 || keyID === 65){
		player.vx = 0.0;		
		console.log("onKeyUp->left");
		e.preventDefault();
	}	
}
function update(){
	player.update();
	player.draw();
	enemy1.update();
	enemy1.draw();
	console.log("updating...");
}

function startDrawing(){
	stopDrawing();
	drawInterval = setInterval(update,fps);
}

function stopDrawing(){
	clearInterval(drawInterval);
}

function Jet(){
	this.srcX 	= jsonData['frames']['jet_0.png']['frame']['x'];
	this.srcY 	= jsonData['frames']['jet_0.png']['frame']['y'];
	this.px		= 100;
	this.py 	= 100;
	this.width 	= jsonData['frames']['jet_0.png']['frame']['w'];
	this.height = jsonData['frames']['jet_0.png']['frame']['h'];
	this.vx 	= 0;
	this.vy 	= 0;
}

Jet.prototype.draw = function(){
	clearJet();
	ctxJet.drawImage(imgSprite,this.srcX,this.srcY,this.width,this.height,this.px,this.py,this.width,this.height);
};

Jet.prototype.update = function(){
	this.px += (dt) * this.vx;
	this.py += (dt) * this.vy;
};

function Enemy(){
	this.srcX 	= jsonData['frames']['enemy_jet_0.png']['frame']['x'];
	this.srcY 	= jsonData['frames']['enemy_jet_0.png']['frame']['y'];
	this.px		= Math.floor(Math.random() * 1000) + GAME_WIDTH;
	this.py 	= Math.floor(Math.random() * GAME_HEIGHT);
	this.width 	= jsonData['frames']['enemy_jet_0.png']['frame']['w'];
	this.height = jsonData['frames']['enemy_jet_0.png']['frame']['h'];
	this.vx 	= -150;
	this.vy 	= 0;
}



Enemy.prototype.draw = function(){
	clearEnemy();
	ctxEnemy.drawImage(imgSprite,this.srcX,this.srcY,this.width,this.height,this.px,this.py,this.width,this.height);
};

Enemy.prototype.update = function(){
	this.px += (dt) * this.vx;
	this.py += (dt) * this.vy;
};



function drawBg(){
	var bgWidth = jsonData["frames"]["bg_1.png"]["frame"]["w"];
	var bgHeight = jsonData["frames"]["bg_1.png"]["frame"]["h"];
	var srcX = jsonData["frames"]["bg_1.png"]["frame"]["x"];
	var srcY = jsonData["frames"]["bg_1.png"]["frame"]["y"];
	ctxBg.drawImage(imgSprite,srcX,srcY,bgWidth,bgHeight,0,0,800,600);
}


function drawSquare(){
	ctxBg.fillStyle = '#505050';
	ctxBg.fillRect(100,100,20,20);
}


function clearCanvas(){
	ctxBg.clearRect(0,0,GAME_WIDTH,GAME_HEIGHT);
}

function clearJet(){
	ctxJet.clearRect(0,0,GAME_WIDTH,GAME_HEIGHT);
}

function clearEnemy(){
	ctxEnemy.clearRect(0,0,GAME_WIDTH,GAME_HEIGHT);
}